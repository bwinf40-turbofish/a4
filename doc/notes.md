# Aufgabe 4: Würfelglück -- Notizen zur Dokumentation (veraltet)

## Grundidee der Implementierung

Jeder Würfel wird mit jedem anderen Würfel verglichen.
Das geschieht in mehreren "Schichten":

* Die innere Schicht ist die konkrete Spielsimulation,
bei der ein Mensch-Ärgere-Dich-Nicht Spiel zwischen zwei Würfeln gespielt wird.
Diese Schicht nennen wir in der Dokumentation und im Quellcode *Spiel* bzw. *Game*.
* Da ein einziges Spiel nicht genug ist, um klar einen besseren Würfel zu küren,
wiederholen wir dieses Spiel immer wieder.
Diese äußere Schicht des Vergleichs nennen wir *Duell* bzw. *Duel*.

### Gesamtablauf

Von ganz außen betrachtet lassen wir für jedes einzelne Würfelpaar ein Duell laufen.
Dieses Duell gibt zurück, welcher der Würfel gewonnen hat.
Nun zählen für jeden Würfel, wie oft er ein solches Duell gewonnen hat.
Der Würfel mit den meisten Duellsiegen ist der beste,
der mit den zweitmeisten der zweitbeste und so weiter.

Es kann vorkommen, dass bei einem Duell kein Sieger ermittelt werden kann.
In diesem Fall bekommt kein Würfel einen Punkt.
Auch in der Endwertung können verschiedene Würfel als gleich gut eingestuft werden;
sie haben gegen gleich viele Würfel das Duell gewonnen.

### Ablauf eines Duells

In einem Duell wird ein Spiel zwischen zwei Würfeln so oft wiederholt,
bis einer der beiden als klarer Gewinner hervorgeht.
Dafür haben wir einen Wert festgesetzt, den es zu erreichen gilt;
der eine Würfel im Positiven, der andere im Negativen.
Das wird dadurch erreicht, dass wir einen allgemeinen Score haben,
der durch den Gewinn von Würfel A um eins inkrementiert wird,
durch den Gewinn von Würfel B dekrementiert,
so dass die beiden Würfel den Score in eine Richtung "ziehen" wie beim Tauziehen.

Da dieses "Tauziehen" theoretisch unendlich lange gehen könnte oder zumindest sehr lange,
haben wir noch einen weiteren Wert eingeführt, die maximale Anzahl an Spielen.
Wenn so viele Spiele simuliert wurden, ohne dass einer der Würfel den Gewinnwert erreicht,
gilt das Duell als unentschieden.

Es gibt also zwei Werte, die bestimmen, wie oft ein Spiel wiederholt werden muss,
damit jemand gewinnt.
Diese Werte richtig zu setzen und abzuwägen ist sehr schwierig.
Je höher sie sind desto genauer ist das Programm, aber es dauert auch länger.

### Ablauf eines Spiels
...

## jut

Es kam die Idee auf, ein System einzuführen, was die Laufzeit des Programms verringern sollte.
Wir wollten nach Abschluss einer einzelnen Spielsimulation einen Wert messen,
welcher angibt, wie groß der Vorsprung des Gewinners zum Verlierer ist;
dieser Wert würde nun in die jeweilige Punktzahl innerhalb eines Duells eingerechnet werden.
Die Idee dahinter ist, dass ein Spiel, in welchem einer der Würfel deutlich dominiert,
gar nicht so oft wiederholt werden müsste.

Obwohl die Laufzeit durch unsere vorläufige Implementierung eines solchen "jut" Wertes tatsächlich
verringert wurde, haben wir nach einiger Überlegung diese Herangehensweise verworfen.
Wir haben nämlich durch das neue Punktesystem den Fokus verschoben:
Unser Programm hat nicht mehr gemessen, welcher Würfel öfter als die anderen gewinnt,
sondern welcher Würfel *besser* gewinnt, also mit größerem Vorsprung die Zielbedingung erfüllt.
Zwar geht das eine oft mit dem anderen einher, aber nicht zwangsläufig.
Es könnte einen Würfel geben, der statistisch nicht so oft gewinnt,
aber wenn er mal gewinnt, dann wahrscheinlich mit großem Vorsprung.
Nach unserem Verständnis sollte dieser Würfel nicht besser gewertet werden
als ein Würfel, der öfter gewinnt, aber vielleicht etwas knapper.

Jede Art von Belohnung eines größeren Vorsprungs ist demnach unfair
und würde das Ergebnis verfälschen.
Also gingen wir zurück zum System, für jeden Sieg *einen* Punkt zu vergeben,
egal wie dieses Spiel gewonnen wurde.
