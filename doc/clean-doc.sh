#!/bin/sh

latexmk -c
rm -rd _minted* 2>/dev/null
rm *.synctex.gz 2>/dev/null

exit 0
