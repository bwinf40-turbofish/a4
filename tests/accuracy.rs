use std::fs;
use std::sync::Arc;

use a4_lib::io;
use a4_lib::solve_a4;
use a4_lib::Die;

fn ten(path: &str) {
    let dice = io::input_file_to_dice(fs::read_to_string(path).unwrap());

    let results = solve_a4(dice.clone().unwrap(), 100, false).sort_by_average();
    let results: Vec<&Arc<Die>> = results.iter().map(|(d, _)| d).collect();
    for _ in 0..10 {
        let new = solve_a4(dice.clone().unwrap(), 100, false).sort_by_average();
        let new: Vec<&Arc<Die>> = new.iter().map(|(d, _)| d).collect();
        assert_eq!(results, new);
    }
}

fn hundred(path: &str) {
    for _ in 0..10 {
        ten(path);
    }
}

#[test]
fn wuerfel0_ten() {
    let path = format!("{}/examples/wuerfel{}.txt", env!("CARGO_MANIFEST_DIR"), 0);
    ten(&path);
}

#[test]
fn wuerfel1_ten() {
    let path = format!("{}/examples/wuerfel{}.txt", env!("CARGO_MANIFEST_DIR"), 1);
    ten(&path);
}

#[test]
fn wuerfel2_ten() {
    let path = format!("{}/examples/wuerfel{}.txt", env!("CARGO_MANIFEST_DIR"), 2);
    ten(&path);
}

#[test]
fn wuerfel3_ten() {
    let path = format!("{}/examples/wuerfel{}.txt", env!("CARGO_MANIFEST_DIR"), 3);
    ten(&path);
}

#[test]
#[ignore]
fn wuerfel0_hundred() {
    let path = format!("{}/examples/wuerfel{}.txt", env!("CARGO_MANIFEST_DIR"), 0);
    hundred(&path);
}

#[test]
#[ignore]
fn wuerfel3_hundred() {
    let path = format!("{}/examples/wuerfel{}.txt", env!("CARGO_MANIFEST_DIR"), 3);
    hundred(&path);
}

#[test]
#[ignore]
fn all_lists_ten() {
    for i in 0..=3 {
        let path = format!("{}/examples/wuerfel{}.txt", env!("CARGO_MANIFEST_DIR"), i);
        ten(&path);
    }
}

#[test]
#[ignore]
fn all_lists_hundred() {
    for i in 0..=3 {
        let path = format!("{}/examples/wuerfel{}.txt", env!("CARGO_MANIFEST_DIR"), i);
        hundred(&path);
    }
}
