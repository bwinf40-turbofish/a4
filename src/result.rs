//! # Ergebnismodul
//!
//! Das `result`-Modul beinhaltet den Teil der Logik des Programms, der die Organisation und
//! Speicherung der verschiedenen Ergebnisse betrifft.

/* IMPORTE */
// Standardbibliothek
use std::cmp::Ordering;
use std::collections::{HashMap, HashSet};
use std::sync::Arc;
// Interne Importe
use crate::game::GameError;
use crate::Die;

/// Ein Alias für den Datentyp eines Ergebniswertes.
pub type ResultValue = f32;

/// Eine Datenstruktur, in der die Ergebnisse des Programms in Form einer Matrix gespeichert werden.
///
/// `ResultMatrix` ist ein Datentyp des Newtype Musters, das heißt, er verschachtelt nur einen
/// anderen Datentyp: Eine zweidimensionale [`HashMap`], die für alle Zweierkombinationen einer
/// gegebenen Liste an Würfeln ([`Die`]) einen Eintrag des Datentyps [`ResultEntry`] speichert.
#[derive(Debug, PartialEq)]
pub struct ResultMatrix(HashMap<Arc<Die>, HashMap<Arc<Die>, ResultEntry>>);

impl ResultMatrix {
    /// Der Konstruktor für den [`ResultMatrix`]-Datentyp.
    ///
    /// # Parameter
    /// - `dice`: Die Würfel ([`Die`]), für die eine (anfangs leere) Ergebnismatrix erstellt werden soll.
    ///
    /// # Rückgabewert
    /// Es wird eine [`HashMap`] erstellt, in der jedem der angegebenen Würfel eine weitere [`HashMap`]
    /// zugeordnet wird, welche wiederum allen anderen Würfeln einen leeren Eintrag des Typs [`ResultEntry`]
    /// zuordnet. Diese wird dann in den Datentyp [`ResultMatrix`] verschachtelt und zurückgegeben.
    pub fn new(dice: &HashSet<Arc<Die>>) -> ResultMatrix {
        let mut results: HashMap<Arc<Die>, HashMap<Arc<Die>, ResultEntry>> =
            HashMap::with_capacity(dice.len());

        for die in dice {
            results.insert(Arc::clone(die), HashMap::new());
            for other in dice {
                results
                    .get_mut(die)
                    .unwrap()
                    .insert(Arc::clone(other), ResultEntry::Empty);
            }
        }

        ResultMatrix(results)
    }

    /// Eine Methode, mit der das Ergebnis eines Duells ([`crate::duel::duel()`]) für zwei gegebene Würfel
    /// ([`Die`]) eingetragen wird.
    ///
    /// # Parameter
    /// - `dice`: Die beiden Würfel, für die etwas eingetragen werden soll.
    /// - `duel_outcome`: Das Ergebnis des Duells, also der Inhalt, den die jeweiligen Einträge haben sollen.
    pub fn insert_duel(
        &mut self,
        dice: (Arc<Die>, Arc<Die>),
        duel_outcome: Result<(ResultValue, ResultValue), GameError>,
    ) {
        let results = &mut self.0;

        let first_entry = results
            .get_mut(&dice.0)
            .expect("Für den ersten Würfel gibt es keinen Eintrag der Ergebnismatrix.")
            .get_mut(&dice.1)
            .expect("Für den zweiten Würfel gibt es keinen Eintrag im Eintrag des ersten Würfels.");
        *first_entry = match duel_outcome {
            Ok((val, _)) => ResultEntry::Valid(val),
            Err(err) => ResultEntry::Invalid(err),
        };

        let second_entry = results
            .get_mut(&dice.1)
            .expect("Für den zweiten Würfel gibt es keinen Eintrag der Ergebnismatrix.")
            .get_mut(&dice.0)
            .expect("Für den ersten Würfel gibt es keinen Eintrag im Eintrag des zweiten Würfels.");
        *second_entry = match duel_outcome {
            Ok((_, val)) => ResultEntry::Valid(val),
            Err(err) => ResultEntry::Invalid(err),
        }
    }

    /// Eine Methode, die die Würfel des [`ResultMatrix`]-Datentyps nach den Durchschnittswerten ebendieser sortiert.
    ///
    /// Alle im [`ResultMatrix`]-Datentyp beinhalteten Würfel ([`Die`]) werden hier mit ihrem durchschnittlichen
    /// Ergebnis gekoppelt und anschließend auch danach sortiert.
    ///
    /// # Rückgabewert
    /// Es wird eine Liste von Tupeln mit der Signatur `(Arc<Die>, ResultValue)` zurückgegeben,
    /// also jeweils eine Arc-Referenz zu einem Würfel und der dazugehörige Durchschnittswert.
    /// Wichtig: Der Würfel mit dem durchschnittlich besten Ergebnis steht ganz vorne in der Liste, der
    /// durchschnittlich schlechteste steht ganz hinten.
    pub fn sort_by_average(&self) -> Vec<(Arc<Die>, ResultValue)> {
        let results = &self.0;
        let mut sorted_results: Vec<(Arc<Die>, ResultValue)> = Vec::new();

        for (die, others) in results {
            let valids: Vec<ResultValue> = others
                .iter()
                .filter(|(_, v)| v.is_valid())
                .map(|(_, v)| v.unwrap())
                .collect();

            let average: ResultValue = if !valids.is_empty() {
                valids.iter().sum::<ResultValue>() / valids.len() as ResultValue
            } else {
                -1.0
            };

            sorted_results.push((Arc::clone(die), average));
        }

        sorted_results.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap_or(Ordering::Equal).reverse());

        sorted_results
    }

    /// Eine Methode, die einen spezifischen Eintrag ([`ResultEntry`]) zurückgibt.
    ///
    /// # Parameter
    /// - `die`: Der Würfel, für den einer der Einträge zurückgegeben werden soll.
    /// - `other`: Der andere Würfel, der angibt, welcher der Einträge des ersten Würfels `die`
    /// zurückgegeben werden soll.
    ///
    /// # Rückgabewert
    /// Wenn es keinen Eintrag für `die` gibt oder -- wenn doch -- in diesem keinen für `other`,
    /// dann gibt die Methode `None` zurück; gibt es den angeforderten Eintrag aber, dann wird er
    /// in ein `Some` verschachtelt zurückgegeben ([`ResultEntry`]).
    pub fn get_entry(&self, die: Arc<Die>, other: Arc<Die>) -> Option<ResultEntry> {
        Some(*self.0.get(&die)?.get(&other)?)
    }
}

/// Ein Enumeration-Datentyp für die verschiedenen Varianten, die ein Eintrag im [`ResultMatrix`]-Datentyp
/// haben kann.
///
/// # Varianten
/// - `Valid`: Der Eintrag ist gültig und der Wert wird als [`ResultValue`] mitgegeben.
/// - `Invalid`: Der Eintrag ist ungültig, wenn ein Duell (`mod@crate::duel`) fehlgeschlagen ist;
/// der Grund wird als [`GameError`] mitgegeben.
/// - `Empty`: Der Eintrag ist weder gültig noch ungültig, sondern einfach leer.
#[derive(Debug, PartialEq, Clone, Copy)]
pub enum ResultEntry {
    Valid(ResultValue),
    Invalid(GameError),
    Empty,
}

impl ResultEntry {
    /// Eine Methode, die zurückgibt, ob der Eintrag einen gültigen Wert enthält.
    fn is_valid(&self) -> bool {
        if let Self::Valid(_) = self {
            return true;
        }
        false
    }

    /// Eine Methode, die den im Eintrag verschachtelten Wert zurückgibt, sofern er gültig ist.
    ///
    /// Verhält sich ähnlich wie die `unwrap()` Methode von `Option`/`Result`.
    ///
    /// # Panics
    /// Das Programm wird mit einem Panic beendet, wenn der Eintrag ungültig oder leer ist, und
    /// sollte dementsprechend vorsichtig benutzt werden.
    fn unwrap(&self) -> ResultValue {
        if let Self::Valid(val) = self {
            *val
        } else {
            panic!("Die `unwrap()` Methode wurde auf einem ungültigen `ResultEntry` aufgerufen.");
        }
    }
}
