//! # Duellmodul
//!
//! Das `duel`-Modul beinhaltet den Teil der Logik des Programms, welcher für das Duell zuständig
//! ist.
//!
//! In einem Duell werden zwei Würfel ([`Die`]) miteinander verglichen, indem eine bestimmte Anzahl von
//! Spielen ([`crate::game`]) zwischen diesen beiden simuliert wird.

/* IMPORTE */
// Standardbibliothek
use std::sync::Arc;
// Interne Importe
use crate::game::{game, GameError};
use crate::result::ResultValue;
use crate::Die;

/// Eine Funktion, die ein Duell zwischen zwei Würfeln ([`Die`]) ausführt.
///
/// In einem Duell wird eine bestimmte Anzahl von Spielen ([`crate::game`]) durchgeführt. Dabei werden die jeweiligen
/// Siegesanteile -- der Quotient aus den Siegen und der Gesamtzahl der Spiele -- errechnet.
///
/// # Parameter
/// - `dice`: Die beiden Würfel, die verglichen werden sollen.
/// - `rounds`: Die Anzahl der Runden bzw. Spiele, die durchgeführt werden sollen.
///
/// # Rückgabewert
/// Im Falle eines gültigen Duells werden die Siegesanteile der beiden Würfel zurückgegeben; dabei
/// entspricht der erste Wert des Rückgabe-Tupels dem Siegesanteil des ersten der eingegebenen
/// Würfel und analog der zweite Wert dem zweiten Würfel. Sollte ein Duell allerdings aufgrund
/// des Auftretens eines unausweichlichen Spielfehlers (siehe [`GameError`]) ungültig sein, wird
/// dieser Fehler zurückgegeben.
pub fn duel(
    dice: (Arc<Die>, Arc<Die>),
    rounds: usize,
) -> Result<(ResultValue, ResultValue), GameError> {
    let mut wins: (usize, usize) = (0, 0);

    // Führe eine angegebene Anzahl von Spielen durch und zähle mit, wer gewinnt
    for round in 0..rounds {
        let game_dice = [Arc::clone(&dice.0), Arc::clone(&dice.1)];
        match game(game_dice, round % 2) {
            Ok(winner) => {
                if dice.0 == winner {
                    wins.0 += 1;
                } else if dice.1 == winner {
                    wins.1 += 1;
                } else {
                    panic!("Das Spiel gibt keinen der eingegebenen Würfel zurück.");
                }
            }
            // Sollte ein "unausweichlicher" Spielfehler auftreten, kann das komplette Duell
            // abgebrochen werden
            Err(err) => {
                if err.is_unavoidable() {
                    return Err(err);
                }
            }
        }
    }

    // Gebe die jeweiligen Siegesanteile zurück
    Ok((
        wins.0 as ResultValue / rounds as ResultValue,
        wins.1 as ResultValue / rounds as ResultValue,
    ))
}
