// IMPORTE

// Standardbibliothek
use std::env;
use std::fs;
use std::process;

// `a4_lib`-Bibliothek
use a4_lib::io;
use a4_lib::solve_a4;

/// Die `main()`-Funktion ist der Einstiegspunkt ins Programm.
///
/// Es wird die Eingabe überprüft und dann an den eigentlichen Lösungsalgorithmus weitergegeben
/// (siehe `a4_lib::solve_a4()`).
/// Die Ergebnisse, die der Algorithmus zurückgibt, werden in eine Tabelle umgewandelt und dann
/// ausgegeben (siehe `a4_lib::result::ResultMatrix` bzw. `a4_lib::io::create_table()`).
///
/// # Kommandozeilenargumente
/// - `eingabedatei`: Die Liste, die die Würfel enthält, mit denen das Programm durchgeführt werden
/// soll.
/// - `genauigkeit`: Ein Wert, der angibt, wie genau das Programm sein soll. Standardmäßig ist
/// dieser auf 50 gesetzt. Im Prinzip ist jede positive Zahl hier möglich;
/// man muss allerdings bedenken, dass mit der Genauigkeit auch die Laufzeit länger wird.
/// Empfohlen sind Werte bis ca. 200.
/// - `A4_SHOW_PROGRESS`: Es gibt die Möglichkeit, sich den Fortschritt des Programms anzeigen zu
/// lassen. Dafür muss diese Umgebungsvariable gesetzt werden.
///
/// # Rückgabewert
/// Wenn bei der Überprüfung der Eingabe ein Fehler entsteht, wird das Programm abgebrochen. Dabei
/// wird ein "exit code" mitgegeben, der eine fehlerhafte Ausführung des Programms signalisiert,
/// und eine Fehlermeldung ausgegeben (diese nutzt die mit dem [`Debug`] trait implementierte Ausgabe
/// des Fehlers, der vom Datentyp `a4_lib::io::InputError` ist).
fn main() -> Result<(), io::InputError> {
    const USAGE: &str =
        "Benutzung: a4 <eingabedatei> (<genauigkeit (50)>)\n(Setze die Umgebungsvariable A4_SHOW_PROGRESS=1, um den Fortschritt anzuzeigen)";

    let mut args = env::args();
    args.next();

    // Speichere den Dateipfad
    let path = args.next().unwrap_or_else(|| {
        // Beende das Programm, wenn kein Dateipfad angegeben wurde
        eprintln!("{}", USAGE);
        process::exit(1);
    });

    // Speichere den Genauigkeitswert
    let accuracy: usize = match args.next().unwrap_or_else(|| "5".to_string()).parse() {
        Ok(num) => match num {
            0 => {
                return Err(io::InputError::new(
                    "Der Genauigkeitswert darf nicht `0` sein.",
                    None,
                ))
            }
            _ => num,
        },
        Err(_) => {
            return Err(io::InputError::new(
                "Der Genauigkeitswert konnte nicht in eine Zahl umgewandelt werden.",
                None,
            ))
        }
    };

    // Beende das Programm, wenn zu viele Argumente angegeben wurden
    if args.next().is_some() {
        eprintln!("{}", USAGE);
        process::exit(1);
    }

    // Prüfe, ob die Umgebungsvariable A4_SHOW_PROGRESS gesetzt wurde
    let show_progress = env::var("A4_SHOW_PROGRESS").is_ok();

    // Wandle die Eingabedatei eine Liste von Würfeln um
    let dice = io::input_file_to_dice(fs::read_to_string(path)?)?;

    // Führe das Programm aus und speichere die entstandene Ergebnismatrix.
    let results = solve_a4(dice, accuracy, show_progress);

    // Wandle die Ergebnisse in eine Tabelle um und gib diese aus
    let table = io::create_table(&results);
    table.printstd();

    // Gib zurück, dass das Programm erfolgreich abgeschlossen wurde
    Ok(())
}
