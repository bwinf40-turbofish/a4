#![allow(rustdoc::private_intra_doc_links)]

//! # A4-Bibliothek
//!
//! Die `a4_lib`-Bibliothek beinhaltet alles, was für das Lösen der BwInf-Aufgabe 4 (Würfelglück)
//! vonnöten ist.
//!
//! Hier ist allerdings nur die Logik des Programms enthalten. Ein- und Ausgabe müssen separat in
//! einer Binary-Crate gehandhabt werden.
//!
//! ## Reexporte
//! Die Bibliothek reexportiert die Würfel-Datenstruktur [`Die`].
//!
//! Dies hat mehrere Vorteile:
//! Zum Einen wird [`Die`] in vielen Modulen benötigt und muss dementsprechend oft importiert werden;
//! der umgeleitete Pfad ist dabei kürzer und intuitiver.
//! Zum Anderen kann die [`Die`] Struktur so auch von außerhalb der Bibliothek erreicht werden -- das
//! [`game`]-Modul, in dem [`Die`] definiert ist, ist privat und verhindert dies so eigentlich.
//! Das bringt den Vorteil, dass man das Programm nicht nur mit einer durch [`io::input_file_to_dice()`]
//! eingelesene Liste starten kann, sondern das [`HashSet`] aus Würfeln des jetzt erreichbaren [`Die`]
//! Datentyps auch selbst erstellen kann. Für unser Programm ist das im Prinzip nicht notwendig, es
//! macht es aber beispielsweise einfacher, Tests für unser Programm zu schreiben.
//! Folgendes ist so möglich:
//! ```
//! use std::collections::HashSet;
//! use a4_lib::Die;
//!
//! let mut dice = HashSet::new();
//! dice.insert(Die::new(vec![1, 2, 3, 4, 5, 6]));
//! dice.insert(Die::new(vec![1, 6]));
//!
//! let results = a4_lib::solve_a4(dice, 7, false);
//! ```

/* MODULE */
mod duel;
mod game;
pub mod io;
mod result;

/* IMPORTE */
// Standardbibliothek
use std::collections::HashSet;
use std::sync::{mpsc, Arc};
// `threadpool`-Bibliothek (um die Anzahl der Threads zu begrenzen)
use threadpool::ThreadPool;
// Interne Importe
use duel::duel;
pub use game::die::Die;
use result::ResultMatrix;

/// Die Anzahl der Threads, die gleichzeitig laufen dürfen.
const WORKERS: usize = 4;

/// Der Faktor, der bestimmt, wie der durch den/die Benutzer:in eingegebene Genauigkeitswert in
/// Duellrunden umgerechnet wird.
const ROUND_FACTOR: usize = 1000;

/// Die Funktion, die die Aufgabe löst.
///
/// Die Funktion ist die Hauptschnittstelle zwischen Bibliothek-Crate und Binary-Crate und
/// beinhaltet den äußersten Ablauf der Problemlösung:
/// Für jede Zweierkombination aus den gegebenen Würfeln ([`Die`]) wird ein Duell ([`duel()`]) gestartet
/// und das Ergebnis aus diesem Duell wird in eine Ergebnismatrix des Datentyps [`ResultMatrix`] eingetragen.
///
/// # Parameter
/// - `dice`: Die gegebenen Würfel aus der Eingabeliste als [`HashSet`].
/// - `accuracy`: Der Genauigkeitswert, der mit [`ROUND_FACTOR`] multipliziert die
/// Duellrundenanzahl bestimmt.
/// - `show_progress`: Es muss festgelegt werden, ob das Programm den Fortschritt anzeigen soll.
///
/// # Rückgabewert
/// Die gesammelten Ergebnisse werden als Datentyp [`ResultMatrix`] zurückgegeben.
///
/// # Parallelität
/// Die einzelnen Duelle werden gleichzeitig ausgetragen, indem sie in verschiedenen Threads
/// ausgeführt werden, was die Laufzeit des Programms maßgeblich verkürzt.
/// Die maximale Anzahl gleichzeitig laufender Threads wird durch einen `Threadpool` der
/// Bibliothek `threadpool` beschränkt (siehe [`WORKERS`]).
pub fn solve_a4(dice: HashSet<Die>, accuracy: usize, show_progress: bool) -> ResultMatrix {
    let dice: HashSet<Arc<Die>> = dice.into_iter().map(Arc::new).collect();
    let pool = ThreadPool::new(WORKERS);
    let (tx, rx) = mpsc::channel();

    // Führe für jedes Würfelpaar ein Duell in einem neuen Thread durch
    let mut others: Vec<Arc<Die>> = Vec::new();
    for die in &dice {
        for other in &others {
            let sender = tx.clone();
            let duel_dice = (Arc::clone(die), Arc::clone(other));
            let duel_dice_return = (Arc::clone(die), Arc::clone(other));
            pool.execute(move || {
                let rounds = accuracy as usize * ROUND_FACTOR;
                let duel = duel(duel_dice, rounds);

                sender.send((duel_dice_return, duel)).unwrap();
            });
        }
        others.push(Arc::clone(die));
    }

    // Erstelle eine leere Ergebnismatrix
    let mut results: ResultMatrix = ResultMatrix::new(&dice);

    let max_duels = ((dice.len() - 1) * dice.len()) / 2; // Gaußsche Summenformel
    let mut duel_counter = 1;

    // Trage die Duellergebnisse in die Matrix ein
    for (duel_dice, outcome) in rx.iter().take(max_duels) {
        results.insert_duel(duel_dice, outcome);
        if show_progress {
            println!("Duell beendet {}/{}", duel_counter, max_duels);
        }
        duel_counter += 1;
    }

    if show_progress {
        println!();
    }

    results
}
