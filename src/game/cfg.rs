//! # Konfigurationsmodul
//!
//! Im `cfg`-Modul sind die Rahmenbedingungen für das Spiel ([`crate::game`]) festgelegt.
//!
//! Hier könnte man auch die Eigenschaften des Spiels abändern, wenn man es wollte.
//! Um beispielsweise mit dem [`crate::game`]-Modul ein Spiel zwischen vier Spielern zu simulieren, müsste man
//! [`NUM_PLAYERS`] auf 4 setzen, zwei weitere Farben und ihre Startfelder zu [`PlayerColor`]
//! hinzufügen, und diese auch in [`COLORS`] entsprechend eintragen.
//! Dies sähe für ein normales "Mensch ärgere Dich nicht"-Spiel mit vier Spielern dann etwa so aus:
//! ```
//! pub const NUM_PLAYERS: usize = 4;
//! pub const COLORS: [PlayerColor; NUM_PLAYERS] =
//!     [PlayerColor::Black, PlayerColor::Yellow, PlayerColor::Green, PlayerColor::Red];
//!
//! pub enum PlayerColor {
//!     Black = 0,
//!     Yellow = 10,
//!     Green = 20,
//!     Red = 30,
//! }
//! ```
//!
//! Für diese Aufgabe sollte jedoch nichts an den Werten geändert werden.

/// Die Anzahl von Feldern auf dem Spielfeld.
pub const NUM_FIELDS: usize = 40;

/// Die Anzahl von Spielfiguren pro Spieler.
pub const NUM_MEEPLES: usize = 4;

/// Die Anzahl von Spielern.
pub const NUM_PLAYERS: usize = 2;

/// Die Würfelaugenzahl, bei der es ein paar Besonderheiten gibt, wenn man sie würfelt.
/// Im Normalfall ist dies die 6.
pub const SPECIAL_NUM: usize = 6;

/// Ein Enumeration-Datentyp für die verschiedenen Spielerfarben.
///
/// Auch wird hier jeder Spielerfarbe ein Startfeld zugeordnet. Dieses wird als Index für das
/// Spielfeld angegeben und muss somit größer als oder gleich 0 und kleiner als
/// [`NUM_FIELDS`] sein.
#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Eq, Ord)]
pub enum PlayerColor {
    Black = 0,
    Green = 20,
}

/// Die Farben der verschiedenen Spieler.
pub const COLORS: [PlayerColor; NUM_PLAYERS] = [PlayerColor::Black, PlayerColor::Green];
