//! # Spielmodul
//!
//! Das `game`-Modul beinhaltet den Teil der Logik des Programms, welcher für das Spiel zuständig
//! ist.
//!
//! Ein Spiel ist die eigentliche, konkrete Simulation eines "Mensch ärgere Dich nicht"-Spiels.
//!
//! Das `game`-Modul ist dahingehend konzipiert, dass es in sich geschlossen und dementsprechend
//! auch vom restlichen Teil des Programms unabhängig ist.
//! Zudem ist die Konfiguration der "Mensch ärgere Dich nicht"-Simulation flexibel und könnte
//! theoretisch abgeändert werden (siehe Submodul [`mod@cfg`]).
//! Man kann das `game`-Modul also quasi als eigene "Mensch ärgere Dich nicht"-Bibliothek
//! betrachten, die das Programm nutzt, um die Aufgabe zu lösen.

/* MODULE */
pub mod cfg;
pub mod die;
mod meeple;
mod player;

/* IMPORTE */
// Standardbibliothek
use std::cell::RefCell;
use std::convert::TryInto;
use std::rc::Rc;
use std::sync::Arc;
// Interne Importe
use die::Die;
use meeple::MeepleId;
use player::Player;

/// Ein Datentyp für das Spielfeld.
///
/// `Board` ist ein Alias für eine vervielfältigbare veränderbare Referenz zu einem Array,
/// das so viele Elemente hat wie die festgelegte Anzahl an Feldern ([`cfg::NUM_FIELDS`]).
/// Jedes dieser Felder kann entweder leer (mit dem Wert `None`) oder durch eine Spielfigur
/// besetzt (mit dem Wert `Some(id)`, wobei `id` vom Typ [`MeepleId`] ist) sein.
pub type Board = Rc<RefCell<[Option<MeepleId>; cfg::NUM_FIELDS]>>;

/// Eine Funktion, die ein Spiel mit einer festgelegten Konfiguration ([`mod@cfg`]) ausführt.
///
/// Die übergeordnete Struktur eines Spiels ist recht simpel: Ein Spieler ([`Player`]) macht seinen Zug
/// -- wenn er die besondere Zahl ([`cfg::SPECIAL_NUM`]) gewürfelt hat, ist er nochmal dran --,
/// dann ist der nächste Spieler am Zug. Dies wird so lange wiederholt, bis einer
/// der Spieler gewonnen hat -- also alle seine Spielfiguren ([`meeple::Meeple`]) im Haus sind --
/// oder ein Fehler auftritt (siehe [`GameError`]).
///
/// # Parameter
/// - `dice`: Die Würfel ([`Die`]), mit denen das Spiel gespielt werden soll; die Anzahl ist durch die
/// Anzahl der Spieler ([`cfg::NUM_PLAYERS`]) festgelegt.
/// - `starter`: Der Index des Würfels im `dice` Array, der den ersten Zug machen soll
///
/// # Rückgabewert
/// Das Spiel kann entweder fehlschlagen oder ein gültiges Ende finden, deshalb ist der
/// Rückgabewert ein `Result`. Im Falle eines gültigen Spiels wird der Siegerwürfel zurückgegeben;
/// sollte aber ein Fehler auftreten, wird ebendieser zurückgegeben (ein solcher Fehler ist vom
/// Datentyp [`GameError`]).
pub fn game(dice: [Arc<Die>; cfg::NUM_PLAYERS], starter: usize) -> Result<Arc<Die>, GameError> {
    // Prüfe, ob die Würfel fehlerhafte Spiele verursachen würden
    let mut contains_special_num = false;
    for die in &dice {
        if die.has_only_special_num() {
            return Err(GameError::DieWithOnlySpecialNum);
        }

        if die.has_special_num() {
            contains_special_num = true;
        }
    }
    if !contains_special_num {
        return Err(GameError::NoDieWithSpecialNum);
    }

    // Erstelle Spielfeld und Spieler
    let board: Board = Rc::new(RefCell::new([None; cfg::NUM_FIELDS]));
    let mut players: [Player; cfg::NUM_PLAYERS] = dice
        .iter()
        .enumerate()
        .map(|(i, d)| Player::new(Arc::clone(d), cfg::COLORS[i], Rc::clone(&board)))
        .collect::<Vec<Player>>()
        .try_into()
        .unwrap();

    // Schreibe die Spielfiguren, die zu Anfang auf dem Spielfeld sind, auf das Spielfeld
    for p in &players {
        p.write_meeples_to_board();
    }

    // Rotiere durch die Spieler und lasse sie einen Zug durchführen
    let mut current = starter % cfg::NUM_PLAYERS;
    let mut stuck_counter = 0;
    'game_loop: loop {
        let player = &mut players[current];
        'player_turn: loop {
            let num = player.die.roll();
            player.make_move(num);

            // Hat ein Spieler gewonnen ist das Spiel beendet
            if player.won {
                break 'game_loop;
            }

            // Wenn der Spieler nicht die besondere Zahl gewürfelt hat oder feststeckt, ist der
            // nächste Spieler am Zug
            if num != cfg::SPECIAL_NUM || player.stuck {
                break 'player_turn;
            }
        }

        // Breche das Spiel ab, wenn alle Spieler gleichzeitig feststecken
        if player.stuck {
            stuck_counter += 1;
            if stuck_counter == cfg::NUM_PLAYERS {
                return Err(GameError::AllPlayersStuck);
            }
        } else {
            stuck_counter = 0;
        }

        current = (current + 1) % cfg::NUM_PLAYERS;
    }

    // Bestimme den Würfel, mit dem gewonnen wurde
    let winners: Vec<&Player> = players.iter().filter(|p| p.won).collect();
    let winner = match *winners.as_slice() {
        [winner] => &winner.die,
        _ => panic!("Ein Spiel mit mehreren Siegern sollte unmöglich sein."),
    };

    Ok(Arc::clone(winner))
}

/// Ein Enumeration-Datentyp für die verschiedenen Fehler, die bei der Durchführung eines Spiels
/// ([`crate::game`]) auftreten können.
///
/// # Varianten
/// - `DieWithOnlySpecialNum`: Wenn ein Spieler ([`Player`]) nur die besondere Zahl ([`cfg::SPECIAL_NUM`]) würfeln
/// kann, ist das Spiel nicht spielbar, da der Spieler immer wieder am Zug ist (würfelt man die
/// besondere Zahl, so ist man nochmal dran), obwohl er mit nur dieser einen Zahl niemals alle
/// Spielfiguren ins Haus bringen könnte -- es sei denn die besondere Zahl ist die 1; dieser Fall ist
/// hier nicht abgedeckt, wird für unser Programm aber auch nicht benötigt.
/// - `NoDieWithSpecialNum`: Wenn keiner der Spieler die besondere Zahl würfeln kann, ist das Spiel
/// auch nicht spielbar, da niemand jemals mehr als eine Spielfigur (nämlich die, die schon zu
/// Anfang dort ist) auf das Spielfeld ([`Board`]) bringen kann.
/// - `AllPlayersStuck`: Es kann vorkommen, dass ein Spieler keinen Zug mehr machen kann, egal was
/// er würfelt. Dies kann zwar auch wieder aufgehoben werden, wenn einer der anderen Spieler etwas
/// auf dem Spielfeld verändert, sind jedoch alle Spieler in der Situation, dass sie sich mit
/// keinem möglichen Wurf bewegen könnten, muss das Spiel abgebrochen werden.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum GameError {
    DieWithOnlySpecialNum,
    NoDieWithSpecialNum,
    AllPlayersStuck,
}

impl GameError {
    /// Gibt zurück, ob der Fehler ([`GameError`]) unausweichlich ist.
    ///
    /// "Unausweichlich" bedeutet hier, dass dieser Fehler bei der gegebenen Kombination an Würfeln
    /// ([`Die`]) bei jeder Wiederholung des gleichen Spiels ([`game()`]) auf jeden Fall noch
    /// einmal auftreten würde, wenn er einmal auftritt.
    ///
    /// # Varianten
    /// - `DieWithOnlySpecialNum`: Dieser Fehler ist unausweichlich, da sich die Würfelseiten nicht
    /// ändern und ein Würfel mit ausschließlich der besonderen Zahl ([`cfg::SPECIAL_NUM`]) stets zu
    /// einem unspielbaren Spiel führt.
    /// - `NoDieWithSpecialNum`: Dieser Fehler ist unausweichlich, da sich die Würfelseiten nicht ändern
    /// und der Fall, dass kein Würfel eine Seite mit der besonderen Zahl hat, stets zu einem
    /// unspielbaren Spiel führt.
    /// - `AllPlayersStuck`: Dieser Fehler ist nicht unausweichlich, da der Zustand eines Spielers
    /// ([`Player`]), sich nicht bewegen zu können, vom Spielverlauf abhängt, der vom Zufall abhängt
    /// und immer anders ist.
    pub fn is_unavoidable(&self) -> bool {
        match self {
            GameError::DieWithOnlySpecialNum => true,
            GameError::NoDieWithSpecialNum => true,
            GameError::AllPlayersStuck => false,
        }
    }
}
