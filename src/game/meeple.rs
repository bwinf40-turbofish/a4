//! # Spielfigurmodul
//!
//! Das `meeple`-Modul beinhaltet den Teil der Logik des Programms, der die Spielfigur betrifft.

/* IMPORTE */
// Standardbibliothek
use std::cmp::Ordering;
// Interne Importe
use crate::game::player::PlayerColor;

/// Ein Alias für den Datentyp einer Spielfigur-ID.
///
/// Eine solche Identifikationsnummer besteht aus zwei Komponenten:
/// - Die Spielerfarbe gibt an, zu welchem Spieler die Figur gehört.
/// - Ein weiterer Wert als `usize` macht die Spielfigur einzigartig und auch die einzelnen Figuren
/// des gleichen Spielers unterscheidbar.
pub type MeepleId = (PlayerColor, usize);

/// Eine Datenstruktur für eine Spielfigur.
///
/// # Felder
/// - `pos`: Die Position der Spielfigur.
/// - `id`: Ein Wert zur eindeutigen Identifikation einer Spielfigur; sollte für jede Spielfigur
/// unterschiedlich gesetzt werden.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Meeple {
    pub pos: MeeplePosition,
    pub id: MeepleId,
}

impl Meeple {
    /// Der Konstruktor für den [`Meeple`]-Datentyp.
    pub fn new(pos: MeeplePosition, id: MeepleId) -> Meeple {
        Meeple { pos, id }
    }
}

/// Eine Implementierung des [`PartialOrd`] traits für den [`Meeple`]-Datentyp.
///
/// Diese wird genutzt, um zu priorisieren, welche Spielfigur gezogen werden soll.
///
/// Die Implementierung ist simpel: Eine Spielfigur mit einer Position, die als weiter
/// fortgeschritten gilt als die einer anderen, wird höher priorisiert. (Siehe [`MeeplePosition`]
/// bzw. [`MeeplePosition::partial_cmp()`].)
///
/// Zu beachten ist, dass nach dieser Implementierung eine weiter fortgeschrittene Spielfigur
/// "größer" ist als eine weniger weit fortgeschrittene. Möchte man nun also eine Liste so
/// sortieren, dass die weitesten Spielfiguren mit der höchsten Priorität vorne sind, muss man
/// "verkehrt herum" sortieren.
impl PartialOrd for Meeple {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.pos.partial_cmp(&other.pos)
    }
}

impl Ord for Meeple {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

/// Ein Enumeration-Datentyp für die verschiedenen Positionen, die eine Spielfigur ([`Meeple`]) haben kann.
///
/// # Varianten
/// - `Outside`: Die Spielfigur ist nicht auf dem Spielfeld.
/// - `Ingame`: Die Spielfigur ist auf dem Spielfeld; auf welchem Feld -- relativ zum Startfeld --
/// wird mitgegeben.
/// - `House`: Die Spielfigur ist im Haus.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum MeeplePosition {
    Outside,
    Ingame(usize),
    House(usize),
}

/// Eine Implementierung des [`PartialOrd`] traits für den [`MeeplePosition`]-Datentyp.
///
/// Durch diese kann angegeben werden, welche Spielfigurposition als weiter fortgeschritten gilt
/// als eine andere.
///
/// Die Implementierung ist folgendermaßen:
/// - Die Position `MeeplePosition::Outside`, die angibt, dass eine Spielfigur sich außerhalb des
/// Spielfelds befindet, ist am "kleinsten", gilt also am wenigsten weit fortgeschritten.
/// - Darauf folgt die Position `MeeplePosition::Ingame(field)`. Diese gibt an, dass sich eine
/// Spielfigur auf dem Spielfeld befindet. Je größer `field` dabei ist, desto "größer"/weiter
/// fortgeschritten gilt dann auch die Position.
/// - Als am weitesten fortgeschritten gilt die Position `MeeplePosition::House(field)`, die angibt,
/// dass sich eine Spielfigur im Haus befindet. Hierbei wird das Haus quasi als Verlängerung des
/// Spielfelds gesehen. Auch hier gilt, je größer `field`, desto weiter fortgeschritten die
/// Position.
///
/// Beispiel:
/// ``` ignore
/// let pos_a = MeeplePosition::Outside;
/// let pos_b = MeeplePosition::Ingame(0);
/// let pos_c = MeeplePosition::Ingame(15);
/// let pos_d = MeeplePosition::House(1);
/// let pos_e = MeeplePosition::House(3);
///
/// assert!(pos_a < pos_b);
/// assert!(pos_b < pos_c);
/// assert!(pos_c < pos_d);
/// assert!(pos_d < pos_e);
/// ```
impl PartialOrd for MeeplePosition {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match self {
            MeeplePosition::Outside => match other {
                MeeplePosition::Outside => Some(Ordering::Equal),
                _ => Some(Ordering::Less),
            },
            MeeplePosition::Ingame(spos) => match other {
                MeeplePosition::House(_) => Some(Ordering::Less),
                MeeplePosition::Ingame(opos) => spos.partial_cmp(opos),
                MeeplePosition::Outside => Some(Ordering::Greater),
            },
            MeeplePosition::House(spos) => match other {
                MeeplePosition::House(opos) => spos.partial_cmp(opos),
                _ => Some(Ordering::Greater),
            },
        }
    }
}

impl Ord for MeeplePosition {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}
