//! # Würfelmodul
//!
//! Im `die`-Modul ist alles, was den Spielwürfel betrifft, definiert.

/* IMPORTE */
// `rand`-Bibliothek (um zufällige Zahlen zu generieren)
use rand::prelude::SliceRandom;
// Interne Importe
use crate::game::cfg;

/// Ein Alias für den Datentyp einer Würfelseite.
pub type DieFace = usize;

/// Eine Datenstruktur für einen Würfel.
///
/// # Felder
/// - `faces`: Die Würfelseiten des Würfels in einer sortierten Liste.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Hash)]
pub struct Die {
    faces: Vec<DieFace>,
}

impl Die {
    /// Der Konstruktor für den [`Die`]-Datentyp.
    ///
    /// # Parameter
    /// - `faces`: Die Würfelseiten, die der Würfel haben soll.
    ///
    /// # Rückgabewert
    /// Die angegebenen Würfelseiten werden sortiert; daraus wird ein Würfel erstellt und
    /// zurückgegeben.
    pub fn new(mut faces: Vec<usize>) -> Die {
        faces.sort_unstable();
        Die { faces }
    }

    /// Eine Methode, die das Würfeln des Würfels simuliert, indem sie eine zufällige Würfelseite
    /// zurückgibt.
    pub fn roll(&self) -> DieFace {
        let mut rng = rand::thread_rng();
        let random_face = self.faces.choose(&mut rng).unwrap();
        *random_face
    }

    /// Eine Getter-Methode für das private `faces` Feld.
    pub fn get_faces(&self) -> &Vec<DieFace> {
        &self.faces
    }

    /// Gibt zurück, ob der Würfel eine Würfelseite mit der besonderen Zahl ([`cfg::SPECIAL_NUM`])
    /// hat.
    pub fn has_special_num(&self) -> bool {
        self.faces.contains(&cfg::SPECIAL_NUM)
    }

    /// Gibt zurück, ob der Würfel ausschließlich Würfelseiten mit der besonderen Zahl
    /// ([`cfg::SPECIAL_NUM`]) hat.
    pub fn has_only_special_num(&self) -> bool {
        for &face in &self.faces {
            if face != cfg::SPECIAL_NUM {
                return false;
            }
        }
        true
    }
}
