//! # Spielermodul
//!
//! Das `player`-Modul beinhaltet den Teil der Logik des Programmes, der den Spieler betrifft.
//!
//! Spieler sind die Instanzen, zwischen denen ein Spiel ausgetragen wird.
//! Der Hauptteil der Logik, die dem Spieler zukommt, ist das Finden des "besten" Zugs und die
//! Ausführung dieses Zugs.
//!
//! ## Reexporte
//! Der Enumeration-Datentyp [`PlayerColor`] aus dem [`mod@cfg`]-Modul wird ins `player`-Modul
//! reexportiert. Dies ist der Fall, da die Spielerfarben zwar Teil der Konfiguration des Spiels
//! sind, semantisch allerdings eher zu dem Spieler und somit in dieses Modul gehören. Das führt zu
//! intuitiveren Import-Pfaden.

/* IMPORTE */
// Standardbibliothek
use std::sync::Arc;
// Interne Importe
use crate::game::cfg;
pub use crate::game::cfg::PlayerColor;
use crate::game::die::{Die, DieFace};
use crate::game::meeple::{Meeple, MeepleId, MeeplePosition};
use crate::game::Board;

/* MACROS */
#[doc(hidden)]
macro_rules! return_if_ok {
    ($e:expr) => {
        let res = $e;
        if res.is_ok() {
            return res;
        }
    };
}

/// Ein Datentyp für das Haus.
///
/// `House` ist ein Alias für ein Array, das so viele Elemente hat wie die festgelegte Anzahl an
/// Spielfiguren ([`cfg::NUM_MEEPLES`]).
/// Jedes dieser Felder kann entweder leer (mit dem Wert `None`) oder durch eine Spielfigur
/// besetzt (mit dem Wert `Some(id)`, wobei `id` vom Typ [`MeepleId`] ist) sein.
type House = [Option<MeepleId>; cfg::NUM_MEEPLES];

/// Ein Alias für das Ergebnis einer Funktion, die nach einem möglichen Zug sucht.
///
/// Wenn ein Zug zurückgegeben werden soll, geschieht das in Form einer Liste mit Zug-Anweisungen
/// (siehe [`MoveInstruction`]); ist jedoch kein Zug möglich, wird ein einfacher Fehler
/// zurückgegeben.
type MoveResult = Result<Vec<MoveInstruction>, ()>;

/// Eine Datenstruktur für einen Spieler.
///
/// # Felder
/// - `die`: Der Würfel des Spielers.
/// - `color`: Die Farbe des Spielers.
/// - `board`: Eine veränderbare Referenz zum Spielfeld.
/// - `meeples`: Die Spielfiguren des Spielers; die Anzahl ist festgelegt ([`cfg::NUM_MEEPLES`]).
/// - `house`: Das Haus des Spielers; das Ziel, in das die Spielfiguren zu bringen sind.
/// - `stuck`: Wenn der Spieler sich nicht mehr bewegen kann, egal was er würfelt, steckt er fest.
/// - `won`: Wenn alle Spielfiguren im Haus sind, hat der Spieler gewonnen.
#[derive(Debug)]
pub struct Player {
    pub die: Arc<Die>,
    pub color: PlayerColor,
    board: Board,
    meeples: [Meeple; cfg::NUM_MEEPLES],
    house: House,
    pub stuck: bool,
    pub won: bool,
}

impl Player {
    /// Der Konstruktor für den [`Player`]-Datentyp.
    ///
    /// # Parameter
    /// - `die`: Der Würfel des Spielers.
    /// - `color`: Die Farbe des Spielers.
    /// - `board`: Eine Referenz zum Spielfeld.
    ///
    /// # Rückgabewert
    /// Der Spieler wird mit den als Argumente gegebenen Werten des Spielfelds, des Würfels und der
    /// Farbe erstellt. Standardmäßig beginnt jeder Spieler mit einer Spielfigur auf dem Startfeld
    /// und alle anderen sind noch gar nicht auf dem Spielfeld. Anfangs ist das Haus des Spielers
    /// komplett leer, er steckt noch nicht fest und hat auch noch nicht gewonnen.
    pub fn new(die: Arc<Die>, color: PlayerColor, board: Board) -> Player {
        let meeples = [
            Meeple::new(MeeplePosition::Ingame(0), (color, 0)),
            Meeple::new(MeeplePosition::Outside, (color, 1)),
            Meeple::new(MeeplePosition::Outside, (color, 2)),
            Meeple::new(MeeplePosition::Outside, (color, 3)),
        ];

        Player {
            die,
            color,
            board,
            meeples,
            house: [None; cfg::NUM_MEEPLES],
            stuck: false,
            won: false,
        }
    }

    /// Eine Methode, die einen Spielzug ausführt.
    ///
    /// Ein Zug läuft folgendermaßen ab:
    /// - Zuerst muss geprüft werden, ob Spielfiguren in der letzten Runde von einem anderen
    /// Spieler vom Spielfeld geworfen wurden ([`Self::check_kicked_meeples`]).
    /// - Dann wird nach dem bestmöglichen konkreten Zug gesucht und, wenn möglich, ausgeführt;
    /// vorher ist es wichtig, die Spielfiguren so zu sortieren, dass priorisierte Positionen
    /// zuerst gezogen werden (siehe [`Meeple`] bzw. [`Meeple::partial_cmp()`]).
    /// - Sollte es mit der gewürfelten Zahl keinen möglichen Zug geben, muss geprüft werden, ob
    /// unabhängig der gewürfelten Zahl überhaupt ein Zug möglich ist; wenn dem nicht so ist,
    /// steckt der Spieler fest.
    /// - Schließlich muss noch überprüft werden, ob der Spieler durch den ausgeführten Zug
    /// gewonnen hat.
    ///
    /// # Parameter
    /// - `num`: Die gewürfelte Zahl, mit der gezogen werden soll.
    pub fn make_move(&mut self, num: DieFace) {
        // Prüfe, ob Figuren vom Spielfeld geworfen wurden und passe ihre Werte gegebenenfalls an
        self.check_kicked_meeples();

        // Sortiere die Spielfiguren nach "Weite auf dem Spielfeld"
        self.meeples.sort_by(|a, b| a.cmp(b).reverse());

        // Finde den "bestmöglichen" Zug und führe ihn aus
        let moved = if let Ok(instructions) = self.find_best_move(num) {
            for instr in instructions {
                match instr {
                    MoveInstruction::Meeple { id, pos } => {
                        for meeple in self.meeples.iter_mut().filter(|p| p.id == id).take(1) {
                            meeple.pos = pos;
                        }
                    }
                    MoveInstruction::Board { index, value } => {
                        self.board.borrow_mut()[index] = value;
                    }
                    MoveInstruction::House { index, value } => {
                        self.house[index] = value;
                    }
                }
            }
            true
        } else {
            false
        };

        // Wenn keine Spielfigur bewegt werden konnte, prüfe,
		// ob ein Zug mit irgendeiner Würfelseite möglich gewesen wäre
        if !moved {
            let mut can_move = false;

            let mut faces = self.die.get_faces().clone();
            faces.sort_unstable();
            faces.dedup();

            for face in faces {
                self.meeples.sort_by(|a, b| a.cmp(b).reverse());
                if self.find_best_move(face).is_ok() {
                    can_move = true;
                    break;
                }
            }

            if !can_move {
                self.stuck = true;
            } else {
                self.stuck = false;
            }
        } else {
            self.stuck = false;
        }

        // Prüfe, ob der Spieler gewonnen hat
        self.won = self
            .meeples
            .iter()
            .filter(|p| matches!(p.pos, MeeplePosition::House(_)))
            .count()
            == cfg::NUM_MEEPLES;
    }

    /// Eine Methode, die den bestmöglichen Spielzug findet.
    ///
    /// Welcher Zug Priorität hat, ist durch die offiziellen Spielregeln von "Mensch ärgere Dich
    /// nicht" festgelegt (es gibt jedoch einige Abänderungen):
    /// - Sollte eine Spielfigur ([`Meeple`]) auf dem eigenen Startfeld stehen, so hat der Zug mit dieser Figur
    /// oberste Priorität und muss getätigt werden, wenn er möglich ist.
    /// - Wenn die besondere Zahl ([`cfg::SPECIAL_NUM`]) gewürfelt wurde und das Startfeld frei ist,
    /// muss eine neue Spielfigur auf das Spielfeld gebracht werden, sofern es noch eine gibt.
    /// - Steht weder eine Spielfigur auf dem Startfeld noch wurde die besondere Zahl gewürfelt, so
    /// ist der Spieler eigentlich frei in seinem Zug. Da dies jedoch im Falle einer Simulation
    /// nicht wirklich praktikabel ist, werden die Spielfiguren einfach in der Reihenfolge, in der
    /// sie gespeichert sind (`self.meeples`), durchlaufen. Da es allerdings gefordert ist, immer mit
    /// der Spielfigur zu ziehen, die am weitesten auf dem Spielfeld fortgeschritten ist, müssen
    /// die Spielfiguren vor einem Aufruf dieser Methode entsprechend sortiert werden (siehe
    /// [`Self::make_move`] und [`Meeple::partial_cmp()`]).
    ///
    /// # Parameter
    /// - `num`: Die Zahl, mit der ein Zug gefunden werden soll.
    ///
    /// # Rückgabewert
    /// In der oben beschriebenen Reihenfolge wird für jede Spielfigur eine oder mehrere der
    /// Submethoden für die Suche nach einem Zug
    /// ([`Self::move_to_board`], [`Self::move_within_board`], [`Self::move_to_house`], [`Self::move_within_house`])
    /// aufgerufen. Der erste mögliche Zug, der von einer dieser Methoden zurückgegeben wird, ist
    /// der bestmögliche und wird als Rückgabewert weitergegeben.
    /// Sollte die Methode einmal durch die Prioritätsliste gelaufen sein, ohne einen möglichen Zug
    /// zu finden, wird ein Fehler zurückgegeben.
    fn find_best_move(&self, num: DieFace) -> MoveResult {
        // Bewege die Spielfigur auf dem Startfeld, wenn dort eine steht
        if let Some(meeple) = self
            .meeples
            .iter()
            .find(|p| p.pos == MeeplePosition::Ingame(0))
        {
            return_if_ok!(self.move_within_board(meeple, num));
        }

        // Bringe eine neue Figur aufs Spielfeld, wenn es noch eine gibt, die draußen wartet
        if num == cfg::SPECIAL_NUM {
            if let Some(meeple) = self
                .meeples
                .iter()
                .find(|p| p.pos == MeeplePosition::Outside)
            {
                return_if_ok!(self.move_to_board(meeple));
            }
        }

        // Ziehe mit der Figur, die am weitesten fortgeschritten ist
        for meeple in self
            .meeples
            .iter()
            .filter(|p| !matches!(p.pos, MeeplePosition::Ingame(0) | MeeplePosition::Outside))
        {
            return_if_ok!(self.move_within_house(meeple, num));
            return_if_ok!(self.move_to_house(meeple, num));
            return_if_ok!(self.move_within_board(meeple, num));
        }

        Err(())
    }

    /// Eine Methode, die prüft, ob und wie eine Spielfigur auf das Spielfeld gebracht werden kann.
    ///
    /// # Parameter
    /// - `meeple`: Die Spielfigur, die versetzt werden soll.
    ///
    /// # Rückgabewert
    /// Sollte die Spielfigur versetzt werden können, gibt die Methode eine Reihe von
    /// Anweisungen ([`MoveInstruction`]) zurück, die beschreiben, welche Aktionen dafür getätigt
    /// werden müssen. Ist der Zug jedoch nicht möglich, wird ein simpler Fehler zurückgegeben.
    fn move_to_board(&self, meeple: &Meeple) -> MoveResult {
        if meeple.pos != MeeplePosition::Outside {
            return Err(());
        }

        let start_field = self.color as usize;

        if self.board.borrow()[start_field].is_some() {
            return Err(());
        }

        let instructions = vec![
            MoveInstruction::Board {
                index: start_field,
                value: Some(meeple.id),
            },
            MoveInstruction::Meeple {
                id: meeple.id,
                pos: MeeplePosition::Ingame(0),
            },
        ];

        Ok(instructions)
    }

    /// Eine Methode, die prüft, ob und wie eine Spielfigur innerhalb des Spielfeld versetzt werden kann.
    ///
    /// # Parameter
    /// - `meeple`: Die Spielfigur, die versetzt werden soll.
    /// - `num`: Die Anzahl der Felder, um die die Spielfigur versetzt werden soll.
    ///
    /// # Rückgabewert
    /// Sollte die Spielfigur versetzt werden können, gibt die Methode eine Reihe von
    /// Anweisungen ([`MoveInstruction`]) zurück, die beschreiben, welche Aktionen dafür getätigt
    /// werden müssen. Ist der Zug jedoch nicht möglich, wird ein simpler Fehler zurückgegeben.
    fn move_within_board(&self, meeple: &Meeple, num: usize) -> MoveResult {
        if let MeeplePosition::Ingame(pos) = meeple.pos {
            let start_field = self.color as usize;
            let pos_post = pos + num;
            let index = (pos + start_field) % cfg::NUM_FIELDS;
            let index_post = (pos_post + start_field) % cfg::NUM_FIELDS;

            if pos_post >= cfg::NUM_FIELDS {
                return Err(());
            }

            if let Some(colliding) = self.board.borrow()[index_post] {
                if colliding.0 == self.color {
                    return Err(());
                }
            }

            let instructions = vec![
                MoveInstruction::Board { index, value: None },
                MoveInstruction::Board {
                    index: index_post,
                    value: Some(meeple.id),
                },
                MoveInstruction::Meeple {
                    id: meeple.id,
                    pos: MeeplePosition::Ingame(pos_post),
                },
            ];

            return Ok(instructions);
        }
        Err(())
    }

    /// Eine Methode, die prüft, ob und wie eine Spielfigur vom Spielfeld ins Haus versetzt werden
    /// kann.
    ///
    /// # Parameter
    /// - `meeple`: Die Spielfigur, die versetzt werden soll.
    /// - `num`: Die Anzahl der Felder, um die die Spielfigur versetzt werden soll.
    ///
    /// # Rückgabewert
    /// Sollte die Spielfigur versetzt werden können, gibt die Methode eine Reihe von
    /// Anweisungen ([`MoveInstruction`]) zurück, die beschreiben, welche Aktionen dafür getätigt
    /// werden müssen. Ist der Zug jedoch nicht möglich, wird ein simpler Fehler zurückgegeben.
    fn move_to_house(&self, meeple: &Meeple, num: usize) -> MoveResult {
        if let MeeplePosition::Ingame(pos) = meeple.pos {
            let start_field = self.color as usize;
            let b_index = (pos + start_field) % cfg::NUM_FIELDS;
            let pos_post = pos + num;

            if pos + num < cfg::NUM_FIELDS {
                return Err(());
            }

            let h_pos = pos_post - cfg::NUM_FIELDS;

            match self.house.get(h_pos) {
                Some(h_field) => {
                    if h_field.is_some() {
                        return Err(());
                    }
                }
                None => return Err(()),
            }

            let instructions = vec![
                MoveInstruction::Board {
                    index: b_index,
                    value: None,
                },
                MoveInstruction::House {
                    index: h_pos,
                    value: Some(meeple.id),
                },
                MoveInstruction::Meeple {
                    id: meeple.id,
                    pos: MeeplePosition::House(h_pos),
                },
            ];

            return Ok(instructions);
        }
        Err(())
    }

    /// Eine Methode, die prüft, ob und wie eine Spielfigur innerhalb des Hauses versetzt werden
    /// kann.
    ///
    /// # Parameter
    /// - `meeple`: Die Spielfigur, die versetzt werden soll.
    /// - `num`: Die Anzahl der Felder, um die die Spielfigur versetzt werden soll.
    ///
    /// # Rückgabewert
    /// Sollte die Spielfigur versetzt werden können, gibt die Methode eine Reihe von
    /// Anweisungen ([`MoveInstruction`]) zurück, die beschreiben, welche Aktionen dafür getätigt
    /// werden müssen. Ist der Zug jedoch nicht möglich, wird ein simpler Fehler zurückgegeben.
    fn move_within_house(&self, meeple: &Meeple, num: usize) -> MoveResult {
        if let MeeplePosition::House(h_pos) = meeple.pos {
            let h_pos_post = h_pos + num;

            match self.house.get(h_pos_post) {
                Some(h_field) => {
                    if h_field.is_some() {
                        return Err(());
                    }
                }
                None => return Err(()),
            }

            let instructions = vec![
                MoveInstruction::House {
                    index: h_pos,
                    value: None,
                },
                MoveInstruction::House {
                    index: h_pos_post,
                    value: Some(meeple.id),
                },
                MoveInstruction::Meeple {
                    id: meeple.id,
                    pos: MeeplePosition::House(h_pos_post),
                },
            ];

            return Ok(instructions);
        }
        Err(())
    }

    /// Eine Methode, die anhand des Spielfeldes prüft, ob Spielfiguren ([`Meeple`]) rausgeworfen
    /// wurden und gegebenfalls ihre Positionen ([`MeeplePosition`]) angleicht.
    fn check_kicked_meeples(&mut self) {
        for meeple in &mut self.meeples {
            if let MeeplePosition::Ingame(pos) = meeple.pos {
                let start_field = self.color as usize;
                let index = (pos + start_field) % cfg::NUM_FIELDS;
                if self.board.borrow_mut()[index] == None {
                    meeple.pos = MeeplePosition::Outside;
                }
            }
        }
    }

    /// Eine Methode, die die Werte der Spielfiguren ([`Meeple`] bzw. [`MeepleId`]) auf das Spielfeld überträgt.
    pub fn write_meeples_to_board(&self) {
        for meeple in &self.meeples {
            if let MeeplePosition::Ingame(pos) = meeple.pos {
                let start_field = self.color as usize;
                self.board.borrow_mut()[(pos + start_field) % cfg::NUM_FIELDS] = Some(meeple.id);
            }
        }
    }
}

/// Ein Enumeration-Datentyp für die verschiedenen Aktionen, die getätigt werden müssen, um einen
/// Spielzug zu vollziehen.
///
/// Diese Aktionen treten oft in Kombination als [`MoveResult`] auf.
///
/// # Varianten
/// - `Meeple`: Es soll eine Änderung an einer der Spielfiguren ([`Meeple`]) vorgenommen werden; die [`MeepleId`] der
/// Spielfigur, um die es sich handelt, und die neue Position ([`MeeplePosition`]), auf die die
/// Spielfigur gesetzt werden soll, werden mitgegeben.
/// - `Board`: Es soll eine Änderung am Spielfeld vorgenommen werden; der Index des Felds, um das
/// es sich handelt, und der neue Wert ([`Option<MeepleId>`][Board]) werden mitgegeben.
/// - `House`: Es soll eine Änderung am Haus vorgenommen werden; der Index des Felds, um das es
/// sich handelt, und der neue Wert ([`Option<MeepleId>`][House]) werden mitgegeben.
#[derive(Debug)]
enum MoveInstruction {
    Meeple {
        id: MeepleId,
        pos: MeeplePosition,
    },
    Board {
        index: usize,
        value: Option<MeepleId>,
    },
    House {
        index: usize,
        value: Option<MeepleId>,
    },
}
