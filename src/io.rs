//! # Eingabe-/Ausgabemodul
//!
//! Das `io`-Modul beinhaltet Funktionen und Datentypen, die das Programm zur Handhabung der
//! Ein- und Ausgabe benötigt.

/* IMPORTE */
// Standardbibliothek
use std::collections::HashSet;
use std::fmt::{Debug, Display};
use std::sync::Arc;
// `prettytable`-Bibliothek (für die Darstellung von Tabellen)
use prettytable::{Cell, Row, Table};
// Interne Importe
use crate::game::cfg;
use crate::game::GameError;
use crate::result::{ResultEntry, ResultMatrix};
use crate::Die;

/// Eine Datenstruktur für die Handhabung von Fehlern bei der Eingabe.
///
/// # Felder
/// - `msg`: Die Fehlermeldung, die erklärt, was der Fehler bei der Eingabe war.
/// - `line`: Es gibt die Option, mitzuspeichern, um welche Zeile der Eingabedatei es sich handelt.
#[derive(Clone, Copy)]
pub struct InputError {
    msg: &'static str,
    line: Option<usize>,
}

impl InputError {
    /// Der Konstruktor für den [`InputError`]-Datentyp.
    pub fn new(msg: &'static str, line: Option<usize>) -> InputError {
        InputError { msg, line }
    }
}

/// Eine explizite Implementierung des [`Debug`] traits für den [`InputError`]-Datentyp.
///
/// Diese Formatierung wird ausgegeben, wenn die `main()`-Funktion mit einem Fehler in Form eines
/// [`InputError`] beendet wird.
impl Debug for InputError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let line = match self.line {
            Some(line) => format!(" [Zeile {}]", line),
            None => String::from(""),
        };

        write!(f, "Eingabe fehlerhaft{}: {}", line, self.msg)
    }
}

/// Eine Implementierung des [`From<std::io::Error>`] traits für den [`InputError`]-Datentyp.
///
/// Dies sorgt dafür, dass ein [`std::io::Error`] -- dieser entsteht zum Beispiel, wenn
/// [`std::fs::read_to_string()`] fehlschlägt -- automatisch in einen [`InputError`] umgewandelt werden
/// kann und spezifiziert, auf welche Weise dies geschieht.
impl From<std::io::Error> for InputError {
    fn from(_: std::io::Error) -> Self {
        InputError::new("Die Datei konnte nicht geöffnet werden.", None)
    }
}

/// Eine Funktion, die eine Eingabeliste in eine Liste aus Würfeln ([`Die`]) umwandelt.
///
/// # Parameter
/// - `file`: Die Eingabeliste als `String`.
///
/// # Rückgabewert
/// Sollte das Einlesen der Liste fehlerfrei vonstatten gehen, wird ein [`HashSet`] zurückgegeben,
/// welches alle Würfel, die in der Eingabeliste stehen, in Form des Datentyps [`Die`] beinhaltet.
/// Wenn die Eingabeliste nicht den Vorgaben entspricht, die auf der
/// [BwInf-Website](https://bwinf.de/bundeswettbewerb/40/1/) angegeben sind,
/// wird das Programm mit einem Fehler des [`InputError`] Typs beendet.
pub fn input_file_to_dice(file: String) -> Result<HashSet<Die>, InputError> {
    let mut lines = file.lines();

    let number_of_dice: usize = match lines.next() {
        Some(line) => match line.parse() {
            Ok(num) => num,
            Err(_) => {
                return Err(InputError::new(
                    "Die erste Zeile enthält nicht eine einzelne Zahl, die die Anzahl der Würfel angibt.",
                    Some(1),
                ))
            }
        },
        None => return Err(InputError::new("Die Datei ist leer.", None)),
    };

    let mut dice = HashSet::new();
    let mut counter = 0;
    for (i, line) in lines.enumerate() {
        let mut split = line.split_whitespace();
        let number_of_faces: usize = match split.next() {
            Some(split) => match split.parse() {
                Ok(num) => num,
                Err(_) => {
                    return Err(InputError::new(
                        "Eine Zeile beginnt nicht mit einer Zahl, die die Anzahl der Würfelseiten angibt.",
                        Some(i + 2),
                    ))
                }
            },
            None => continue,
        };

        let mut faces = Vec::new();
        for x in split {
            match x.parse() {
                Ok(num) => faces.push(num),
                Err(_) => {
                    return Err(InputError::new(
                        "Eine Würfelseite konnte nicht in eine Zahl umgewandelt werden.",
                        Some(i + 2),
                    ))
                }
            }
        }

        if faces.len() != number_of_faces {
            return Err(InputError::new(
                "Die Anzahl der angegebenen Würfelseiten passt nicht zu der am Anfang der Zeile angegebenen Zahl.",
                Some(i + 2),
            ));
        }

        dice.insert(Die::new(faces));

        counter += 1;
    }

    if counter != number_of_dice {
        return Err(InputError::new(
            "Die Anzahl der angegebenen Würfel passt nicht zu der in der ersten Zeile angegebenen Zahl",
            Some(1),
        ));
    }

    Ok(dice)
}

/// Eine Funktion, die die Ergebnisse des Programms in eine schön formatierte Tabelle umwandelt.
///
/// # Parameter
/// - `results`: Eine Referenz zu den Ergebnissen als Datentyp [`ResultMatrix`].
///
/// # Rückgabewert
/// Es wird eine Tabelle des Datentyps `Table` aus der `prettytable`-Bibliothek zurückgegeben, in
/// der alle simulierten Ergebnisse samt der Durchschnittswerte der Ergebnisse der einzelnen Würfel
/// ([`Die`]) zusammengefasst und dargestellt sind.
pub fn create_table(results: &ResultMatrix) -> Table {
    let mut table = Table::new();
    let sorted_results = results.sort_by_average();

    let mut header: Vec<Cell> = sorted_results
        .iter()
        .enumerate()
        .map(|(i, _)| Cell::new(format!("Würfel {}", i + 1).as_str()))
        .collect();
    header.insert(0, Cell::new("Würfel"));
    header.insert(1, Cell::new("Würfelseiten"));
    header.push(Cell::new("Durchschnitt"));

    table.set_titles(Row::new(header));

    for (i, (die, av)) in sorted_results.iter().enumerate() {
        let mut row = Vec::with_capacity(sorted_results.len() + 1);

        let die_cell = Cell::new(format!("Würfel {}", i + 1).as_str());
        row.push(die_cell);

        const WIDTH: usize = 20;
        let mut faces_string = String::new();
        let mut last_break = 0;
        for face in die.get_faces() {
            let face = face.to_string();
            if (faces_string.len() + face.len()) - last_break > WIDTH {
                faces_string.pop().unwrap();
                faces_string.push('\n');
                last_break = faces_string.len();
            }
            faces_string.push_str(&(face + " "));
        }

        let faces_cell = Cell::new(faces_string.as_str());
        row.push(faces_cell);

        for (other, _) in sorted_results.iter() {
            let cell = Cell::new(
                results
                    .get_entry(Arc::clone(die), Arc::clone(other))
                    .unwrap()
                    .to_string()
                    .as_str(),
            );
            row.push(cell);
        }

        let average = if *av >= 0.0 {
            format!("{}%", (av * 100.0).round())
        } else {
            String::from("Ungültig")
        };

        row.push(Cell::new(average.as_str()));

        table.add_row(Row::new(row));
    }

    table
}

/// Eine Implementierung des [`Display`] traits für den Typ [`ResultEntry`] aus dem [`crate::result`]-Modul.
/// Dies dient dazu, den Inhalt eines [`ResultMatrix`] Objekts darstellen zu können.
impl Display for ResultEntry {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let printable = match self {
            Self::Valid(val) => format!("{}%", (val * 100.0).round()),
            Self::Invalid(err) => match err {
                GameError::DieWithOnlySpecialNum => {
                    format!(
                        "Ungültig!\nWürfel hat nur {}.",
                        cfg::SPECIAL_NUM
                    )
                }
                GameError::NoDieWithSpecialNum => {
                    format!(
                        "Ungültig!\nKein Würfel hat {}.",
                        cfg::SPECIAL_NUM
                    )
                }
                _ => panic!("Dieser GameError darf nicht zu einem ungültigen Spiel führen."),
            },
            Self::Empty => String::from(""),
        };

        write!(f, "{}", printable)
    }
}
